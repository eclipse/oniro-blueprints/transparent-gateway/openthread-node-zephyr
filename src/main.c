/*
 * SPDX-FileCopyrightText: Huawei Inc.
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Oniro Gateway Blueprint OpenThread node example application
 */

#include <logging/log.h>
LOG_MODULE_REGISTER(openthreadnode, LOG_LEVEL_DBG);

#include <errno.h>
#include <zephyr.h>
#include <sys/printk.h>
#include <stdio.h>
#include <zephyr/usb/usb_device.h>

#define APP_BANNER "Oniro Gateway Blueprint OpenThread node"

void main(void)
{
	LOG_INF(APP_BANNER);

	if (usb_enable(NULL)) {
		return -1;
	}
}
